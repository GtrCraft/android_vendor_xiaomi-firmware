# Firmware
PRODUCT_COPY_FILES += \
    vendor/xiaomi-firmware/phoenixin/abl.elf:install/firmware-update/abl.elf \
    vendor/xiaomi-firmware/phoenixin/aop.mbn:install/firmware-update/aop.mbn \
    vendor/xiaomi-firmware/phoenixin/BTFM.bin:install/firmware-update/BTFM.bin \
    vendor/xiaomi-firmware/phoenixin/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
    vendor/xiaomi-firmware/phoenixin/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
    vendor/xiaomi-firmware/phoenixin/devcfg.mbn:install/firmware-update/devcfg.mbn \
    vendor/xiaomi-firmware/phoenixin/dspso.bin:install/firmware-update/dspso.bin \
    vendor/xiaomi-firmware/phoenixin/hyp.mbn:install/firmware-update/hyp.mbn \
    vendor/xiaomi-firmware/phoenixin/imagefv.elf:install/firmware-update/imagefv.elf \
    vendor/xiaomi-firmware/phoenixin/km4.mbn:install/firmware-update/km4.mbn \
    vendor/xiaomi-firmware/phoenixin/logo.img:install/firmware-update/logo.img \
    vendor/xiaomi-firmware/phoenixin/multi_image.mbn:install/firmware-update/multi_image.mbn \
    vendor/xiaomi-firmware/phoenixin/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
    vendor/xiaomi-firmware/phoenixin/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
    vendor/xiaomi-firmware/phoenixin/storsec.mbn:install/firmware-update/storsec.mbn \
    vendor/xiaomi-firmware/phoenixin/tz.mbn:install/firmware-update/tz.mbn \
    vendor/xiaomi-firmware/phoenixin/uefi_sec.mbn:install/firmware-update/uefi_sec.mbn \
    vendor/xiaomi-firmware/phoenixin/xbl_config.elf:install/firmware-update/xbl_config.elf \
    vendor/xiaomi-firmware/phoenixin/xbl.elf:install/firmware-update/xbl.elf \

